# i3-maximize-hack

### Prequisites:
- [jq](https://stedolan.github.io/jq/)
- workspace named "fullscreen" in i3 config
- gnome-terminal(Yeah, I know...)

### Usage:
- Place toggle_maximize.sh in folder of choice
- chmod +x toggle_maximize.sh
- bind to a key or a button